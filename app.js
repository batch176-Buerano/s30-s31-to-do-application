//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");


//[SECTION] Server Setup
	//esstablish a connection
	const app = express();
	//define a path/address
	const port = 4000; //Assign the port 4000 to avoid conflict in the future with applications that un on port 3000

//[SECTION] Database Connection
	//Connect to MongoDB Atlas
	//change the password bracket to your own database password
	//Get the credectials of your atlas user.
	mongoose.connect('mongodb+srv://joshjames123:admin123@cluster0.ldn4y.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//option to add to avoid deprecation warning because of mongoose/mongodb update
		useNewUrlParser:true,
		useUnifiedTopology:true

	});

	//Ceate notofication if the connection to the db is a success or a failure.

	let db = mongoose.connection;
	//Let's add an on() method from our mongiise connection to show if the connection has succeeded or failed in both the terminal and in the browser for our clinet.
	db.on('error', console.error.bind(console,"Connection Error"));
	//once the connection is open and successfull, we will output a message in the terminal
	db.once('open', () => console.log("Connected to MongoDB"));

	//Middleware - A middleware, in expressjs context, are methods, functions that acts a nd adds features our application.
	app.use(express.json());

	//Schema
	//before we van create documents from our api to sabe into iur ddatabase, we first have to determine the structure of the documents to be written in our database. this is to ensure the consistency of our documents and avoid future errors.

	//Schema() cacts as a blueprint for our data/document.
	
	//Schema() constructor from mongoose to create a new schema object
	const taskSchema = new mongoose.Schema({

		name: String,
		status: String

	})

	//Mongoose Model
	//Models are used to connect your api to the corresponding collection in your database. It is a representation of your collection
	//SYNTASK: mongoose.model(<nameofCollectionInAtlas>,<schemaToFFlollow>)

	const Task = mongoose.model("task", taskSchema);

	//Post route to create a new task
	app.post('/tasks', (req,res) => {
		//temp response to terminla.
		//console.log(req.body);


		let newTask = new Task({

			name: req.body.name,
			status: req.body.status

		});

		//to save the mothod
/*		newTask.save((error,savedTask)=>{
			if (error) {
				res.send("Document creation failed.");
			} else {
				res.send(savedTask);
			}
		});*/

		newTask.save()
		.then(result => res.send({message: "Document Creation Succesful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));
	});



//Get Method Request to retrive All task document from our Collection,

app.get('/tasks', (req,res)=>{

	Task.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

})


//sample Schema
	const sampleSchema = new mongoose.Schema({
		name: String,
		isActive: Boolean
	})

	const Sample = mongoose.model("samples",sampleSchema);

	app.post('/samples',(req,res) => {

		let newSample = new Sample({

			name: req.body.name,
			isActive: req.body.isActive

		})

/*		newSample.save((error,savedSample) => {
			if (error) {
				res.send("error");
			} else {
				res.send(savedSample);
			}
		})*/

		newSample.save()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})

	app.get('/samples', (req,res)=>{

	Sample.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

})


//Schema

/*const manggagamitSchema = new mongoose.Schema({
		username: String,
		isAdmin: Boolean
	})

	const Manggagamit = mongoose.model("manggagamit",manggagamitSchema);

	app.post('/mgaManggagamit',(req,res) => {

		let newManggagamit = new Manggagamit({

			username: req.body.username,
			isAdmin: req.body.isAdmin

		})

		newManggagamit.save((savedManggagamit,error) => {
			if (error) {
				res.send(error);
			} else {
				res.send(savedManggagamit);
			}
		});
	});*/



//Activity for s30 =============================

		const userSchema = new mongoose.Schema({
			userName: String,
			password: String
		})

		const User = mongoose.model("users",userSchema);

		app.post('/users',(req,res) => {

			let newUser = new User({

				userName: req.body.userName,
				password: req.body.password

			})

			newUser.save()
			.then(result => res.send({message: "User Added to Collection"}))
			.catch(error => res.send({message: "Error. Need to enter userName and password"}));
		})

		app.get('/users', (req,res)=>{

		User.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error));

	})





//[SECTION] Entry Point Respose
	//bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port ${port}`));
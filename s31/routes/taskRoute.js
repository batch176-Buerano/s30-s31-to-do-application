//Contains all the endpoints for our application
//We separate the route sch that "app.js" only contains information on the server

const express = require("express")

//Create a Router instant that functions as a midleware and routing system.
//Allow access to HTTP method midlewares that makes it easier to create route for our application
const router = express.Router();

const taskController = require("../controllers/taskControllers")

//[Routes]
//Route for getting all the tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})


//Route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})


//Route for Deleting a task
	//call out the routing component to register a brand new endpoint.
	//When ontegrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC.
	//2 types of End Point
		//Static Route
			//=> unchanging, fixed, constant, steady
		//Dynamic
			//=> interchangeable or NOT FIXED

router.delete("/:task", (req, res) => {
	//identifyy the task to be executed within this endpoint
	//call out the proper function for this route and identify the source/provider of the function.
	//DETERMINE wether the value inside the path variable is transmitted to the server.
	//console.log(req.params.task);
	//place the value of the path variable inside its own container.
	let taskId = req.params.task;
	//res.send('Hello from delete'); // this is only to check if the setup is correct.

	//retrive the identity of the task by inserting the ObjectId if the resouse
	//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path variable.
	//Path variable -> this will allow us to insert data within the scope of the URL.
		//what is a variable? -> container, storage of information
	//When is a path variable useful? 
		//when inserting only a sigle piece of information.
		//this is also usefull when passing down information to REST API method that does NOT include a BODY section like 'GET'.

	//upon executing this method a promise will be initualized so we need to handle the result of the promise in our rroute module.
	taskController.deleteTask(taskId).then(resultNgDelete => res.send(resultNgDelete));
});

//Route for Updating task status (Pending -> Complete)
	//Lets create a dynamic endpoint for this brand new route.

router.put('/:task', (req, res) => {
	//check if you are able to acquire the values inside the variable.
	console.log(req.params.task);

	let idNiTask = req.params.task;

	//identify the business logic behind this task inside the controller module.
	//coll the intended controller to execute the process, make sure to identify the provider /source of the function.
	//after invoking the controller method handle the outcome.
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
})


//Route for updating task status (Complete -> Pending)
	//this will be counter procedure for the previous task.

router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	//console.log(id);

	//declere the business logic aspect of this brand new task in our app.
	//invoke the task you want to execute in this route.
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	})
})


//Use "module.export" to export the router object to use in the "app.js".
module.exports = router;
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 4000;

app.use(express.json());
// app.use(express.urlencoded({extended: true}));


mongoose.connect('mongodb+srv://joshjames123:admin123@cluster0.ldn4y.mongodb.net/toDo176?retryWrites=true&w=majority',{

	useNewUrlParser:true,
	useUnifiedTopology:true

});

let db = mongoose.connection;
db.on('error', console.error.bind(console,"Connection Error"));
db.once('open', () => console.log("Connected to MongoDB"));

//Add the task route
//example: localhost:4000/task/
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Server running at port: ${port}`));
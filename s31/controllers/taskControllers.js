//Controllers - contains the functions and business logic of our Express JS Application.

const Task = require("../models/task");

//[CONTROLLERS]

//[SECTION] - Create
	module.exports.createTask = (requestBody) => {

		let newTask = new Task({
			name: requestBody.name
		})

		return newTask.save().then((task,error) => {
			if (error) {
				return false
			} else {
				return task
			}

		})
	}


//[SECTION] - Retrive
	module.exports.getAllTasks = () => {

		return Task.find({}).then(result => {
			return result
		})
	}

//[SECTION] - Destroy
	//1 Remove an existing resource inside the task collection.
		//expose the data accross other modules othe app so that it will become reusable.
		//would we need an input from the user? -> yes- which resource you want to target

	module.exports.deleteTask = (idNgTask) => {
		//how will the data will be processed in otder to execute the task
		//select which mongoose method will be used in order to acquire the desire and goal.
		//Mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas.
		//in order to identify thhe location in which the function will be executed append the model name.

		//findByIDAndRemove => this is a mongoose methdod which target a document using it's id field and removes the targeted document from the collection.
		//upon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state in may fall on.
		//Promises in JS
			//=> Pending (waiting to be executed)
			//=> Fulfilled (successfully executed)
			//=> Rejected (unfillfilled promise)
		//to be able to handle the possible states upon executing the task.
		//we are going to insert a 'thenable expression to determin HOW we will respond depending in the result of the promise.
		//identify the 2 possible state of the promise using a then expression.
		return Task.findByIdAndRemove(idNgTask).then((fulfilled, rejected) => {
			//identify how you will act according to outcome.
			if (fulfilled) {
				return 'The task has been successfully Removed';
			} else {
				return 'Fail to remove Task';
			};
			//assign a new endpoint for this route.
		});
	};


//[SECTION] - Update
	//1. Change status of Task. pending -> 'complited'.
	//we will reference the documents using its ID field
	module.exports.taskCompleted = (taskId) => {
		//Query/search for the desired task to update.
		//The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of request.
		//upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to gandle the possible outcome of that promise.
		return Task.findById(taskId).then((found, error) => {
			//describe how were going to handle the outcome of the promise using a selection contole structure.
			//error -> rejected state of the promise
			//process the document found form the collection and change the status from 'pending' to 'completed'.


			if (found) {
				//call out the result that describes the result if the query  when successful.
				console.log(found); //document found the db should be displayed in the terminal.
				//modified the status of the return document to completed.
				found.status = 'Completed';
				//Save the new changes inside our database.
				//upon saving the new changes for this document a 2nd promise will be instantiated.
				//we wil chain a thenable expression upon performing a save() method into our returned document
				return found.save().then((updatedTask, saveErr) => {
					//catch the state of the promise to identify a specific response.
					if (updatedTask) {
						return 'Task has been successfully modified';
					} else {
						//display the actual error
						return 'Task failed to Update';
					}
				})
			} else {
				return 'Error. No Match Found';
				
			}
		})
	};


	//2. Change the status of the task (completed to pending)

	module.exports.taskPending = (userInput) => {
		//expose this new component
		//search the database for the user input.
		//findById mongoose method -> will run a search query inside our database using the id field as it's reference.
		//since performing this method will have 2 possible states/outcome, we will chain a then expression to handle the posible states of the promise.
		// assign this new controller task to its own separrate route
		return Task.findById(userInput).then((result, err) => {
			if (result) {

				result.status = 'Pending';

				return result.save().then((taskUpdated, error) => {
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`;
					} else {
						return 'Error when saving task updates';
					}
				});

			} else {
				return 'Somthing went wrong';
			}
		})

	}